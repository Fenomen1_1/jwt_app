<?php


namespace App\Repositories;



use App\Models\Notes;
use Illuminate\Support\Facades\Redis;

class NoteRepository extends BaseRepository
{
    protected $mainClass = Notes::class;

    public function getAllNotes()
    {
        return Notes::all();
    }
    public function create($note)
    {;
        $note->save();
    }
    public function deleteNote($id)
    {
        return Notes::where('id', $id)->delete();
    }

    public function findOneByName($name)
    {
        return Notes::query()
            ->where('name', '=', $name)
            ->first();
    }

    public function findOneById($id)
    {
        return Notes::query()
            ->where('id', '=', $id)
            ->first();
    }

    public function update($note, array $data)
    {
            $note->update($data);
            return $note;
    }

}
