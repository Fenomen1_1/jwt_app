<?php

namespace App\Http\Controllers\Notes;



use AllowDynamicProperties;
use App\Http\Controllers\Controller;
use App\Services\Notes\NoteService;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function createNote(Request $request)
    {
        $noteService  = new NoteService();

        return $noteService->createNote($request->only(['title', 'description', 'author']));
    }
    public function updateNote(Request $request, $id)
    {
        $noteService  = new NoteService();
        return $noteService->updateNote($request->only(['title', 'description', 'author']), $id);
    }
    public function deleteNote($id)
    {
        $noteService  = new NoteService();
        return $noteService->deleteNote($id);
    }
    public function readNotes()
    {
        $noteService  = new NoteService();
        return $noteService->readNotes();
    }

}
