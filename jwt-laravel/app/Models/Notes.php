<?php

namespace App\Models;

use App\Services\Notes\DTO\NoteDTO;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    use HasFactory;

    protected $table = 'notes';
    protected $fillable = ['title', 'description', 'author'];

    public function setInfo(NoteDTO $noteDTO)
    {
        $this->title = $noteDTO->getTitle() ?? null;
        $this->description = $noteDTO->getDescription() ?? null;
        $this->author = $noteDTO->getAuthor() ?? null;
    }
}
