<?php

namespace App\Services\Notes\DTO;


use AllowDynamicProperties;

#[AllowDynamicProperties] class NoteDTO
{
    public function setInfo($note)
    {
        $this->title = $note['title'] ?? null;
        $this->description = $note['description']?? null;
        $this->author = $note['author'] ?? null;
    }

    public function getInfo(): array
    {
        return
            [
                'title' => $this->getTitle(),
                'description' => $this->getDescription(),
                'author' => $this->getAuthor(),
            ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }
    public function getDescription(): string
    {
        return $this->description;
    }
    public function getAuthor(): string
    {
        return $this->author;
    }

}
