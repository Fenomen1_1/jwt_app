<?php

namespace App\Services\Notes;

use App\Models\Notes;
use App\Repositories\NoteRepository;
use App\Services\Notes\DTO\NoteDTO;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
class NoteService
{
    protected $NoteRepository;

    public function __construct() {
        $this->NoteRepository = new NoteRepository();
    }
    public function createNote($data)
    {
        // Создание заметки
        $note = new Notes();
        $noteDTO = new NoteDTO();
        $noteDTO->setInfo($data);
        $note->setInfo($noteDTO);
        $this->NoteRepository->create($note);

        Log::info('Заметка успешно создана', ['note_id' => $note->id]);
        return response()->json(['message' => 'Заметка успешно создана', 'note' => $note], 201);
    }

    public function updateNote($data, $id)
    {
        // Проверяем, существует ли заметка
        $note = $this->NoteRepository->findOneById($id);
        if (!$note) {
            Log::error('Заметка не найдена', ['note_id' => $id]);
            return response()->json(['error' => 'Заметка не найдена'], 404);
        }

        $this->NoteRepository->update($note,$data);

        Log::info('Заметка успешно обновлена', ['note_id' => $note->id]);
        return response()->json(['message' => 'Заметка успешно обновлена', 'note' => $note]);
    }


    public function deleteNote($id)
    {
        $deleted = $this->NoteRepository->deleteNote($id);

        if ($deleted) {
            Log::info('Заметка успешно удалена', ['note_id' => $id]);
            return response()->json(['message' => 'Заметка успешно удалена'], 200);
        }
        Log::error('Заметка не найдена', ['note_id' => $id]);
        return response()->json(['error' => 'Заметка не найдена'], 404);
    }

    public function readNotes()
    {
        return $this->NoteRepository->getAllNotes();
    }

}
