<?php

namespace Tests\Unit;

use Tests\TestCase;

class NoteApiTest extends TestCase
{
    /**
     * Тест создания заметки через API.
     *
     * @return void
     */
    public function testCreateNote()
    {

        $response = $this->postJson('/create', [
            'title' => 'Test Note213',
            'description' => 'This is a test note3213',
            'author' => 'Akezhan123'
        ]);
        $response->assertStatus(201)
            ->assertJson(['message' => 'Заметка успешно создана']);
    }

    /**
     * Тест обновления заметки через API.
     *
     * @return void
     */
    public function testUpdateNote()
    {
        // Предположим, что у нас есть ID заметки, которую мы хотим обновить
        $noteId = 1;

        $response = $this->putJson("update/{$noteId}", [
            'title' => 'Test Note new',
            'description' => 'This is a test note',
            'author' => 'Damir'
        ]);
        $response->assertStatus(200)
            ->assertJson(['message' => 'Заметка успешно обновлена']);
    }

    /**
     * Тест удаления заметки через API.
     *
     * @return void
     */
    public function testDeleteNote()
    {
        // Предположим, что у нас есть ID заметки, которую мы хотим удалить
        $noteId = 1;

        $response = $this->deleteJson("delete/{$noteId}");
        $response->assertStatus(200)
            ->assertJson(['message' => 'Заметка успешно удалена']);
    }

    /**
     * Тест чтения заметок через API.
     *
     * @return void
     */
    public function testReadNotes()
    {
        $response = $this->getJson('read');
        $response->assertStatus(200);
    }
}
