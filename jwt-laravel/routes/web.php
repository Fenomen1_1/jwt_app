<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Notes\NotesController;
use App\Http\Middleware\ValidateAndHandleErrors;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


//Route::post('login', [AuthController::class, 'login']);
//Route::post('register', [AuthController::class, 'register']);
//Route::post('me', [AuthController::class, 'me']);
//
//Route::name('user')->group(function (){
//    Route::view('/view','private')->middleware('auth')->name('private');
//
//    Route::get('/login', function (){
//        if(Auth::check()){
//            return ['Good'];
//        }
//        return view('login');
//    })->name('login');
//});


Route::prefix('auth')->middleware('api')->controller(AuthController::class)->group(function (){
    Route::post('login','login');
    Route::post('user','user');
    Route::post('logout','logout');
    Route::post('refresh','refresh');
});











Route::middleware([ValidateAndHandleErrors::class])->group(function () {
    Route::post('create', [NotesController::class, 'createNote']);
    Route::put('update/{id}', [NotesController::class, 'updateNote']);
    Route::delete('delete/{id}', [NotesController::class, 'deleteNote']);
    Route::get('read', [NotesController::class, 'readNotes'])->withoutMiddleware([ValidateAndHandleErrors::class]);;
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
